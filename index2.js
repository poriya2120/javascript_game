document.addEventListener('DOMContentLoaded',()=>{

    const grid=document.querySelector('.grid')
    const scoreDisplay=document.getElementById('score')
    const width=8;
    const squares=[]
    let score=0
    const candyColors=[
        'red',
        'yellow',
        'orange',
        'purple',
        'green',
        'blue'
    ]
    function createBord(){
        for(let i=0 ;i< width*width; i++){
            const square=document.createElement('div')
            square.setAttribute('draggable',true)
            square.setAttribute('id',i)
            let randomColor=Math.floor(Math.random()*candyColors.length)
            square.style.backgroundColor=candyColors[randomColor]
            grid.appendChild(square)
            squares.push(square)
        }
    }
    
    createBord()
    
    let colorBenigDragged
    let colorBenigReplaced
    let squareIdBeingDragged
    let squreIdBeingReplaced
    
    squares.forEach(square=>square.addEventListener('dragstart',dragStart))
    squares.forEach(square=>square.addEventListener('dragend',dragEnd))
    squares.forEach(square=>square.addEventListener('dragover',dragOver))
    squares.forEach(square=>square.addEventListener('dragenter',dragEnter))
    squares.forEach(square=>square.addEventListener('dragleave',dragLeave))
    
    squares.forEach(square=>square.addEventListener('drop',dragDrop))
    
    function dragStart(){
        colorBenigDragged=this.style.backgroundColor
        console.log(colorBenigDragged);
        squareIdBeingDragged=parseInt(this.id)
        console.log(this.id,'dragstart')
    
    }
    
    function dragEnd(){
        console.log(this.id,'dragEnd')
        let validMoves=[
            squareIdBeingDragged -1,
            squareIdBeingDragged-width,
            squareIdBeingDragged +1,
            squareIdBeingDragged +width
        ]
    
        let validMove=validMoves.includes(squreIdBeingReplaced)
        if(squreIdBeingReplaced && validMove){
            squreIdBeingReplaced=null
        }
        else if(squreIdBeingReplaced && !validMove){
            squares[squreIdBeingReplaced].style.backgroundColor=colorBenigReplaced
            squares[squareIdBeingDragged].style.backgroundColor=colorBenigDragged
    
        }
        else squares[squareIdBeingDragged].style.backgroundColor=colorBenigDragged
    }
    
    function moveDown(){
    for(i=0;i<55;i++){
        if(squares[i+width].style.backgroundColor===''){
            squares[i+width].style.backgroundColor=squares[i].style.backgroundColor
            squares[i].style.backgroundColor=''
        }
    }
    
    }
    function checkRowForFour(){
        for(i=0;i<60;i++){
            let rowOfFour=[i,i+1,i+2,i+3]
            let decidedColor=squares[i].style.backgroundColor
            const isBalank=squares[i].style.backgroundColor===''
            const noValid=[5,6,7,13,14,15,21,22,23,30,31,37,38,39,45,46,47,53,54,55]
            if(noValid.includes(i)) continue
            if(rowOfFour.every(index=>squares[index].style.backgroundColor===decidedColor && !isBalank)){
                score +=4
                scoreDisplay.innerHTML=score
                rowOfFour.forEach(index=>{
                    squares[index].style.backgroundColor=''
                })
            }
        }
    }
    
    checkRowForFour()
    
    
    ///-------
    function checkColumnForFour(){
        for(i=0;i<47;i++){
            let columnOfFour=[i,i+width,i+width*2,i+width*3]
            let decidedColor=squares[i].style.backgroundColor
            const isBalank=squares[i].style.backgroundColor===''
            if(columnOfFour.every(index=>squares[index].style.backgroundColor===decidedColor && !isBalank)){
                score +=4
                scoreDisplay.innerHTML=score
    
                columnOfFour.forEach(index=>{
                    squares[index].style.backgroundColor=''
                })
            }
        }
    }
    
    checkColumnForFour()
    function checkRowForThree(){
        for(i=0;i<61;i++){
            let rowOfThree=[i,i+1,i+2]
            let decidedColor=squares[i].style.backgroundColor
            const isBalank=squares[i].style.backgroundColor===''
            const noValid=[6,7,14,15,22,23,30,31,38,39,46,47,54,55]
            if(noValid.includes(i)) continue
            if(rowOfThree.every(index=>squares[index].style.backgroundColor===decidedColor && !isBalank)){
                score +=3
                scoreDisplay.innerHTML=score
    
                rowOfThree.forEach(index=>{
                    squares[index].style.backgroundColor=''
                })
            }
        }
    }
    
    checkRowForThree()
    
    
    ///-------
    function checkColumnForThree(){
        for(i=0;i<47;i++){
            let columnOfThree=[i,i+width,i+width*2]
            let decidedColor=squares[i].style.backgroundColor
            const isBalank=squares[i].style.backgroundColor===''
            if(columnOfThree.every(index=>squares[index].style.backgroundColor===decidedColor && !isBalank)){
                score +=3
                scoreDisplay.innerHTML=score
    
                columnOfThree.forEach(index=>{
                    squares[index].style.backgroundColor=''
                    const FristRow=[0,1,2,3,4,5,6,7]
                    const isFristRow=FristRow.includes(i)
                    if(isFristRow && squares[i].style.backgroundColor===''){
                        let reandomColor=Math.floor(Math.random()*candyColors.length)
                        squares[i].style.backgroundColor=candyColors[randomColor]
                    }
                })
            }
        }
    }
    
    checkColumnForThree()
    //------------
    
    ////--------
    
    window.setInterval(function(){
    
        moveDown
        checkRowForFour()
        checkColumnForFour()
        checkRowForThree()
        checkColumnForThree()
      
    },100)
    
    function dragOver(e){
        e.preventDefault()
        console.log(this.id,'dragOver')
        
    }
    
    function dragEnter(e){
        e.preventDefault()
    
        console.log(this.id,'dragEnter')
        
    }
    
    function dragLeave(){
        console.log(this.id,'dragLeave')
        
    }
    
    function dragDrop(){
        console.log(this.id,'dragdrop')
        colorBenigReplaced=this.style.backgroundColor
        squreIdBeingReplaced=parseInt(this.id)
        squares[squareIdBeingDragged].style.backgroundColor = colorBenigReplaced
    
        //squares[squreIdBeingDragged].style.backgroundColor=colorBenigReplaced
        
    }
    
    
    
    
    })